package com.example.securitydemo.configuration;

import com.example.securitydemo.service.UserService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public SecurityConfiguration(PasswordEncoder passwordEncoder, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.httpBasic().and().authorizeRequests()
                .antMatchers("/api/v1/user/all", "/api/v1/user/register").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/api/v1/user/admin/**").hasRole("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/api/v1/user/user/**").hasRole("USER")
                .and().authorizeRequests()
                .anyRequest().authenticated();
//                .and()
//                .exceptionHandling().accessDeniedPage("/403")
//                .and()
//                .logout().permitAll();
        //httpSecurity.csrf().disable();
        //        // dont authenticate this particular request
        //        httpSecurity.authorizeRequests().antMatchers("/authenticate", "/register").permitAll();
        //
        //        httpSecurity.authorizeRequests().antMatchers("/users/**").hasAnyRole("USER", "ADMIN");
        //        httpSecurity.authorizeRequests().antMatchers("/admins/**").hasRole("ADMIN");
        //        httpSecurity.authorizeRequests().anyRequest().authenticated();
        //
        //        httpSecurity.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

    }
}
