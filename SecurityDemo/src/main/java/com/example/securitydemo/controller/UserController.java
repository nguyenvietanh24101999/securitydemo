package com.example.securitydemo.controller;

import com.example.securitydemo.entity.User;
import com.example.securitydemo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@RequestBody User user) {
        try {
            return new ResponseEntity<>(userService.addUser(user), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<User>> allUser() {
        try {
            return new ResponseEntity<>(userService.getAllUser(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null);
        }
    }

    @GetMapping("/admin")
    public String adminPage() {
        try {
            return "this is page for role admin";
        } catch (Exception e) {
            return "your role is not admin";
        }
    }

    @GetMapping("/user")
    public String userPage() {
        try {
            return "this is page for role user";
        } catch (Exception e) {
            return "your role is not user";
        }
    }
}
